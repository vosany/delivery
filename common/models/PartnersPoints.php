<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partners_points".
 *
 * @property integer $id
 * @property integer $partner_id
 * @property string $lat
 * @property string $lng
 */
class PartnersPoints extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners_points';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['partner_id', 'lat', 'lng'], 'required'],
            [['partner_id'], 'integer'],
            [['lat', 'lng'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partner ID',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }
}
