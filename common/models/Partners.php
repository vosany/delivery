<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "partners".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $commision
 * @property integer $distance
 * @property string $carname
 * @property integer $maxweight
 * @property integer $maxsize
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','license'], 'required'],
            [['user_id', 'commision', 'distance', 'maxweight', 'maxsize','status'], 'integer'],
            [['carname','current_lat','start_point','end_point','current_lon','license'], 'string', 'max' => 255],
        ];
    }
    
    public function getUser()
    {
		return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    
    public function getProfile()
    {
		return $this->hasOne(Userprofile::className(), ['user_id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'commision' => Yii::t('app', 'Commision'),
            'distance' => Yii::t('app', 'Distance from point'),
            'carname' => Yii::t('app', 'Model car'),
            'maxweight' => Yii::t('app', 'Max weight'),
            'maxsize' => Yii::t('app', 'Max size'),
            'start_point' => Yii::t('app', 'Start way point'),
            'end_point' => Yii::t('app', 'End way point'),
            'license' => Yii::t('app', 'License'),
            'status' => Yii::t('app', 'status'),
        ];
    }
}
