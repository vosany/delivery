var currentPage = '';

$(document).ready(function(){
  
	if($('.current-page-set').val() != undefined){
		currentPage = $('.current-page-set').val();
		var from_coord = jQuery.parseJSON(from_coordinates);
		var to_coord = jQuery.parseJSON(to_coordinates);
		setDirection(from_coord.lat,from_coord.lng,to_coord.lat,to_coord.lng)
	}
	$('button.abudabi').on("click",function(event){
		event.preventDefault();
		map.setCenter(new google.maps.LatLng(24.464053, 54.381088));
		map.setZoom(12);
	});
	$('button.dubai').on("click",function(event){
		event.preventDefault();
		map.setCenter(new google.maps.LatLng(25.206197, 55.255922));
		map.setZoom(12);
	})
  
});

	var newid=0;
	var markers = [];
window.initMap = function initMap() {
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 8,
		navigationControl: false,
		mapTypeControl: false,
		streetViewControl:false,
		minZoom:7,
		maxZoom:18,
		scaleControl: false,
		center: {lat: 26.045311, lng: 52.443253}
	});
	var geocoder = new google.maps.Geocoder();
	map.addListener('click', function(event) {
		var latlng = {lat: event.latLng.lat(), lng: event.latLng.lng()};
		codeAddress(geocoder,map,latlng);
	});
	
	var allowedBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(25.7, 51.6), 
            new google.maps.LatLng(22.9, 56.2));
            
	var lastValidCenter = map.getCenter();  
  
	
	if(typeof partnerPointSet !== 'undefined'){
		for (i=0; i<partnerPointSet.length; i++)
		{
			var marker = setMarker({lat:parseFloat(partnerPointSet[i].lat),lng:parseFloat(partnerPointSet[i].lng)});
			markers[marker.id] = marker;
			markLis(marker);					
		}
	}
}
	
	function sentToApiAndChangeMap(address,setZoom = 16)
	{
		var urlToCheck = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		$.post(urlToCheck,function(data){
			if(data.status == 'OK'){
				if(data.results[0] != undefined){
					var resAddr = data.results[0];
					
					map.setCenter(resAddr.geometry.location);
					map.setZoom(setZoom);
					setMarker(resAddr.geometry.location);
				}
			}		  
		});
	}  
  
	function setMarker(latlng,mtitle= '')
	{ 
		newid+=1;
		marker = new google.maps.Marker({
			position: {
				lat: latlng.lat,
				lng: latlng.lng
			},
			id: newid,
			map: map,
			//label: mtitle,
		});
	    
		return marker;
	}
	function markLis(marker)
	{
		google.maps.event.addListener(marker, "click", function (point) {delMarker(marker.id)});	
	}
	var delMarker = function(id) {
		var answer = confirm("Delete point?");
		if(answer){
		    var marker = markers[id];
		    marker.setMap(null);
		    $('.pPoint'+id).remove();
	    }
	}

	
	function setDirection(from_lat,from_lon,to_lat,to_lon){
			var directionsDisplay = new google.maps.DirectionsRenderer();
			var directionsService = new google.maps.DirectionsService();
			directionsDisplay.setMap(map);
			directionsDisplay.setOptions( { suppressMarkers: true, suppressInfoWindows: true } );	
		
			var start_point = new google.maps.LatLng(from_lat, from_lon);
			var end_point = new google.maps.LatLng(to_lat,to_lon);
		
		
			marker = new google.maps.Marker({
				position: start_point,
				icon: 'https://www.delivery.devnull.pp.ua/images/go-icon-small.png',
				map: map
			});
			
			marker = new google.maps.Marker({
				position: end_point,				
				icon: 'https://www.delivery.devnull.pp.ua/images/finish-icon-small.png',
				map: map
			});		
		
			google.maps.event.addListener(marker, 'click', function () {
				infowindow.open(map, this);
			});	
			var waypts = [];		
			var request = {
			origin: start_point,
			destination: end_point,
			waypoints: waypts,
			optimizeWaypoints: true,
			travelMode: 'DRIVING',
			unitSystem: google.maps.UnitSystem.METRIC,
			provideRouteAlternatives: true,
			avoidHighways: false,
			avoidTolls: true
			};
			directionsService.route(request, function(result, status) { 
			 if (status == google.maps.DirectionsStatus.OK) {
			  directionsDisplay.setDirections(result);
			  var routes = result.routes;
			  var leg = routes[0].legs;
			  var lenght = leg[0].distance.text;
			  var duration = leg[0].duration.text;
				$('#orders-total_km').val(lenght);
			  infowindow = new google.maps.InfoWindow({ content: 'Distance: '+lenght+'<br>Duration: '+duration });
			  infowindow.open(map, marker);
			 }
			});
	}



var partnerPoints = 0;
function codeAddress(geocoder,map,latlng) {	
	geocoder.geocode({'location': latlng}, function(results, status) {
		var answer = confirm("Add point?");
	    if (status === 'OK' && answer) {
	      if (results[1]) {
	      	if($('.PartnersPoints').length > 0)
	      		partnerPoints = $('.PartnersPoints').length / 2;
	      	if(results[0]){
	      		if($('#partner-start-point').val().length == 0)
	      			$('#partner-start-point').val(results[0].formatted_address);
	      		else if($('#partner-end-point').val().length == 0)
	      			$('#partner-end-point').val(results[0].formatted_address);
	      	}
	        //map.setZoom(11);
	        var marker = setMarker(latlng);//new google.maps.Marker({position: latlng,map: map});
	        markers[marker.id] = marker;
	        markLis(marker);
	        var innerInputPartner = '<input type="hidden" class="PartnersPoints" name="PartnersPoints['+partnerPoints+'][lat]" value="'+ latlng.lat +'">';
	        innerInputPartner += '<input type="hidden" class="PartnersPoints" name="PartnersPoints['+partnerPoints+'][lng]" value="'+ latlng.lng +'">'
	        $('.partners-set-route').append(innerInputPartner);
	      } else {
	        window.alert('No results found');
	      }
	    } else {
	      //window.alert('Geocoder failed due to: ' + status);
	    }
	  });
  }