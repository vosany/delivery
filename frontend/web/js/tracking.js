var map;
var mapto;
var googleMapType; 
var marker;  

window.initMap = function initMap() {
	//googleMapType = google.maps.MapTypeId.HYBRID;
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 12,
		/*scrollwheel: false,*/
		navigationControl: false,
		mapTypeControl: false,
		/*disableDoubleClickZoom: true,*/
		streetViewControl:false,
		mapTypeId: googleMapType,
		minZoom:7,
		maxZoom:18,
		/*zoomControl:false,*/
		scaleControl: false,
		/*draggable: false,*/
		center: {lat: position.lat, lng: position.lon}
	});
	setMarker(position);
	var allowedBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(25.7, 51.6), 
            new google.maps.LatLng(22.9, 56.2));
            
	var lastValidCenter = map.getCenter();         
  
  
	var markers = [];

	var geocoder = new google.maps.Geocoder();
  

  
  function setMarker(latlng,mtitle= '')
  { 
    	  marker = new google.maps.Marker({
	      position: {
	        lat: latlng.lat,
	        lng: latlng.lon
	      },
	      icon: 'https://www.delivery.devnull.pp.ua/images/cur_big.png',
	      map: map,
				//label: mtitle,
	    });
    	return marker;
  }

}