var userCheckPhone;
var findMeClicked = false;
$(document).ready(function(){
  $('.btn-adress-choose').on("click",function(e){
    var buttonType = $(this).attr('data-type');
    if(buttonType == 'use-my-place'){
    	$('#orders-address_type').val('use');
    	$('.places-blocks-to-show').hide();
    	$('.use-my-place-block').show();
    }
    if(buttonType == 'add-my-place'){
    	$('#orders-address_type').val('add');
    	$('.places-blocks-to-show').hide();
    	$('#add-my-place-block').show();
    }
    return false;
  });
  $('.orders-form btn').on("click",function(){
    if($('#orders-total_km').val().length == 0){
    	$('.check-me').click();
    }
  });
	$('#orders-phone_to').on("keyup",function(){
		var chekingPhone = '';
		var currentPhoneInput = $(this);
		currentPhoneInput.next().hide();
		if($(this).val().length > 9){
			$.post('/orders/checkphone',{phone:$(this).val()},function(data){
				var obj = $.parseJSON( data );
				userCheckPhone = obj;
				//if(obj.length > 0){
					$.each(obj, function(index, value){
						chekingPhone += '<a href="#" class="selectCheckedUser" data-id="'+value.id+'">'+value.first_name+' '+value.last_name+'<a>';
					});
				//}
				
				currentPhoneInput.next().html(chekingPhone);
				currentPhoneInput.next().show();
			})    	
		}
	});
	
	$(document).on("click",'.checkPhone a', function(e){
	  e.preventDefault();
	  var userData = userCheckPhone[$(this).attr('data-id')];
	  $('#orders-first_name_to').val(userData.first_name);
	  $('#orders-last_name_to').val(userData.last_name);
	  $('#orders-address_to').val(userData.address);
	  
	  $(this).parent().hide();
	});
	
	$('#orders-address_from').on("keyup",function(){
	  var addressFrom = $(this).val();
	  
	  if(addressFrom.length > 5){
	  	$.post('/orders/getlocaddr',{address:addressFrom},function(data){
	  		var cityData = JSON.parse(data);
	  		if(cityData.status == 'OK'){
	  			if(cityData.results[0] != undefined){
	  				var maxCount = 5;
	  				if(cityData.results.length < 5)
	  					maxCount = cityData.results.length;
	  				var cityHtml = '';
	  				for (var i=0; i<maxCount; i++)
	  				{
	  					var locData = cityData.results[i];
	  					var addressInput = '';
	  					if(locData.address_components.length > 4)
	  						addressInput = locData.address_components[1].long_name + ' ' + locData.address_components[0].long_name;	  					
	  					else
	  						addressInput = locData.address_components[0].long_name;
						cityHtml += '<a class="searched-address_from" href="#" data-lat="'+locData.geometry.location.lat+'" data-lng="'+locData.geometry.location.lng+'" data-addr="'+addressInput+'">'+cityData.results[i].formatted_address+'</a>';
	  				}
	  				if(cityHtml.length > 0){
			  			$('.field-orders-address_from .inform-block').html(cityHtml);
			  			$('.field-orders-address_from .inform-block').show();
			  		}
	  			}
	  		}
	  		
	  	});
	  }
	});
	
	$(document).on("click",'.searched-address_from',function(event){
	  event.preventDefault();
	  
	  var curLat = $(this).attr('data-lat');
	  var curLng = $(this).attr('data-lng');
	  var lat = parseFloat(curLat);
	  var lng = parseFloat(curLng);
	  
	  $('#orders-from_lat').val(curLat);
	  $('#orders-from_lon').val(curLng);
	  map.setCenter(new google.maps.LatLng(curLat, curLng));
	  
	  map.setZoom(23);
	  var opo = setMarker({lat,lng});	
	  
	  $('#orders-address_from').val($(this).attr('data-addr'));
	  $(this).parent().hide();
	  return false;
	});
	
	$('#orders-address_to').on("keyup",function(){
	  var addressFrom = $(this).val();
	  
	  if(addressFrom.length > 5){
	  	$.post('/orders/getlocaddr',{address:addressFrom},function(data){
	  		var cityData = JSON.parse(data);
	  		if(cityData.status == 'OK'){
	  			if(cityData.results[0] != undefined){
	  				var maxCount = 5;
	  				if(cityData.results.length < 5)
	  					maxCount = cityData.results.length;
	  				var cityHtml = '';
	  				for (var i=0; i<maxCount; i++)
	  				{
	  					var locData = cityData.results[i];
	  					var addressInput = '';
	  					if(locData.address_components.length > 4)
	  						addressInput = locData.address_components[1].long_name + ' ' + locData.address_components[0].long_name;	  					
	  					else
	  						addressInput = locData.address_components[0].long_name;
						cityHtml += '<a class="searched-address_to" href="#" data-lat="'+locData.geometry.location.lat+'" data-lng="'+locData.geometry.location.lng+'" data-addr="'+addressInput+'">'+cityData.results[i].formatted_address+'</a>';
	  				}
	  				if(cityHtml.length > 0){
			  			$('.field-orders-address_to .inform-block').html(cityHtml);
			  			$('.field-orders-address_to .inform-block').show();
			  		}
	  			}
	  		}
	  		
	  	});
	  }
	});
	
	$(document).on("click",'.searched-address_to',function(event){
		event.preventDefault();
		var curLat = $(this).attr('data-lat');
		var curLng = $(this).attr('data-lng');
		var lat = parseFloat(curLat);
		var lng = parseFloat(curLng);
		
		$('#orders-to_lat').val(curLat);
		$('#orders-to_lon').val(curLng);
		
		$('#orders-address_to').val($(this).attr('data-addr'));
		$(this).parent().hide();
	  
	  
		var address_to_data = $('#orders-address_to').val();
		var address_from_data = $('#orders-address_from').val();
		var address_from_lat = $('#orders-from_lat').val();
		var address_from_lon = $('#orders-from_lon').val();
		
		if(address_to_data.length > 10){
			checkDirection(address_to_data,address_from_lat,address_from_lon)
		}
	  
	  return false;
	});
});
var map;
var mapto;
var googleMapType; 
var marker;  
window.initMap = function initMap() {
	//googleMapType = google.maps.MapTypeId.HYBRID;
	map = new google.maps.Map(document.getElementById('map'), {
		zoom: 8,
		/*scrollwheel: false,*/
		navigationControl: false,
		mapTypeControl: false,
		/*disableDoubleClickZoom: true,*/
		streetViewControl:false,
		mapTypeId: googleMapType,
		minZoom:7,
		maxZoom:18,
		/*zoomControl:false,*/
		scaleControl: false,
		/*draggable: false,*/
		center: {lat: 24.256497, lng: 54.470963}
	});

	mapto = new google.maps.Map(document.getElementById('mapto'), {
		zoom: 8,
		/*scrollwheel: false,*/
		navigationControl: false,
		mapTypeControl: false,
		/*disableDoubleClickZoom: true,*/
		streetViewControl:false,
		mapTypeId: googleMapType,
		minZoom:7,
		maxZoom:18,
		/*zoomControl:false,*/
		scaleControl: false,
		/*draggable: false,*/
		center: {lat: 24.256497, lng: 54.470963}
	});
	
	var allowedBounds = new google.maps.LatLngBounds(
		new google.maps.LatLng(25.7, 51.6), 
		new google.maps.LatLng(22.9, 56.2));
            
	var lastValidCenter = map.getCenter();         
    
	var markers = [];

	var geocoder = new google.maps.Geocoder();
	
	$('#coordinates-city_id').on("change",function(){
		var selectedCity = $('#coordinates-city_id option:checked').text();
		sentToApiAndChangeMap(selectedCity,12);
	});
  
  
  $('#coordinates-adress').on("blur",function() {
  	var selectedCity = $('#coordinates-city_id option:checked').text();
  	var typedStreet = $(this).val();
  	sentToApiAndChangeMap(selectedCity+'+'+typedStreet,16);
    //map.setCenter(new google.maps.LatLng(25.161968, 55.218180));
    //map.setZoom(10);
	});
	
	$('#coordinates-house').on("blur",function() {
  	var selectedCity = $('#coordinates-city_id option:checked').text();
  	var typedStreet = $('#coordinates-adress').val();
  	var typedHouse = $(this).val();
  	sentToApiAndChangeMap(selectedCity+'+'+typedStreet+'+'+typedHouse,20);
	});


	function sentToApiAndChangeMap(address,setZoom = 16)
	{
		var urlToCheck = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		$.post(urlToCheck,function(data){
			if(data.status == 'OK'){
				if(data.results[0] != undefined){
					var resAddr = data.results[0];
					
					map.setCenter(resAddr.geometry.location);
					map.setZoom(setZoom);
					setMarker(resAddr.geometry.location);
				}
			}		  
		});
	}
  
  
  google.maps.event.addListener(map, 'click', function(event) { 
  	geocoder.geocode({
	    'latLng': event.latLng
	  }, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	      if (results[0]) {
	        console.log(results[0].geometry.location);
	      }
	    }
	  });
  
  });
	

		
/*
var waypts = [];
waypts.push({
  location: new google.maps.LatLng(24.488413, 54.630539),
  stopover: true
});
waypts.push({
  location: new google.maps.LatLng(24.375689, 54.731529),
  stopover: true
});*/
 		
		$('.check-me').on("click",function(){
			//$('#mapto').show();
			//google.maps.event.trigger(mapto, 'resize');
			var address_to_data = $('#orders-address_to').val();
			var address_from_data = $('#orders-address_from').val();
			var address_from_lat = $('#orders-from_lat').val();
			var address_from_lon = $('#orders-from_lon').val();
			
			if(address_to_data.length > 10){
				checkDirection(address_to_data,address_from_lat,address_from_lon)
			}
			else{
				alert("Just type your address");
			}
		});

	/*
		$('#orders-address_to').on('keyup',function(){
			currentAddress = $(this).val();
			if(currentAddress.length > 4){
				var geocoder = new google.maps.Geocoder(); 
				geocoder.geocode( { 'address':currentAddress}, function(results, status) {console.log(results);});
				var urlToCheck = 'https://maps.googleapis.com/maps/api/geocode/json?address='+currentAddress+'&language=en&key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		  	$.post(urlToCheck,function(data){
					if(data.status == 'OK'){
						if(data.results[0] != undefined){
							var resAddr = data.results;
							console.log(urlToCheck);
						}
					}		  
				});
			}
		});
		*/

		var findMeButton = $('.find-me');
		// Check if the browser has support for the Geolocation API
		if (!navigator.geolocation) {
		
		  findMeButton.addClass("disabled");
		  $('.no-browser-support').addClass("visible");
		
		} else {
		
		  findMeButton.on('click', function(e) {
		  	findMeClicked = true;
		    e.preventDefault();
		    navigator.geolocation.getCurrentPosition(function(position) {
		      // Get the coordinates of the current possition.
		      var lat = position.coords.latitude;
		      var lng = position.coords.longitude;
		
		      $('#orders-from_lat').val(lat.toFixed(3));
		      $('#orders-from_lon').val(lng.toFixed(3));
		      $('.coordinates').addClass('visible');
		
		      // Create a new map and place a marker at the device location.
		      //map.setCenter(new google.maps.LatLng(25.161968, 55.218180));
		      map.setCenter(new google.maps.LatLng(lat, lng));
		      var oblink = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='+lat+','+lng+'&language=en&key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		      
		      $.post(oblink,function(data){
						if(data.status == 'OK'){
							if(data.results[0] != undefined){
								var locationAddress = data.results[0].formatted_address;
								var res = locationAddress.split(",");
								$('#orders-address_from').val(locationAddress);
								$('#coordinates-adress').val(res[0]);
								$('#coordinates-house').val(res[1]);
								//console.log(res);
								$('.check-me').prop('disabled', false);
							}
						}		  
					});
		      
		      
				map.setZoom(23);
				var opo = setMarker({lat,lng});					
		    });
				
		  });
		}
}
	function checkDirection(address_to_data,address_from_lat,address_from_lon)
	{
		var urlToCheck = 'https://maps.googleapis.com/maps/api/geocode/json?address='+address_to_data+'&language=en&key=AIzaSyCSDBUqBwp7recbKeONWV5akmaRDw1MXI0';
		$.post(urlToCheck,function(data){
			if(data.status == 'OK'){
				if(data.results[0] != undefined){
					var resAddr = data.results[0];
					
					if(address_from_lat != undefined && address_from_lon != undefined){
						//setMarker({lat:address_from_lat,lng:address_from_lon});
					}
					$('#orders-to_lat').val(resAddr.geometry.location.lat);
					$('#orders-to_lon').val(resAddr.geometry.location.lng);
					setDirection(address_from_lat,address_from_lon,resAddr.geometry.location.lat,resAddr.geometry.location.lng);
					
					//setMarker(resAddr.geometry.location,resAddr.formatted_address);

					//map.setCenter(resAddr.geometry.location);
					//map.setZoom(12);
				}
			}		  
		});
	}
	function setDirection(from_lat,from_lon,to_lat,to_lon){
			var directionsDisplay = new google.maps.DirectionsRenderer();
			var directionsService = new google.maps.DirectionsService();
			directionsDisplay.setMap(mapto);
			directionsDisplay.setOptions( { suppressMarkers: true, suppressInfoWindows: true } );	
		
			var start_point = new google.maps.LatLng(from_lat, from_lon);
			var end_point = new google.maps.LatLng(to_lat,to_lon);
		
			if(marker)
				marker.setMap(null);
		
			marker = new google.maps.Marker({
				position: start_point,
				map: mapto
			});
			
			marker = new google.maps.Marker({
				position: end_point,
				map: mapto
			});		
		
			google.maps.event.addListener(marker, 'click', function () {
				infowindow.open(mapto, this);
			});	
			var waypts = [];		
			var request = {
				origin: start_point,
				destination: end_point,
				waypoints: waypts,
				optimizeWaypoints: true,
				travelMode: 'DRIVING',
				unitSystem: google.maps.UnitSystem.METRIC,
				provideRouteAlternatives: true,
				avoidHighways: false,
				avoidTolls: true
			};
			directionsService.route(request, function(result, status) { 
			 if (status == google.maps.DirectionsStatus.OK) {
			 	if(findMeClicked)
					$('.find-me').click();
				else{					
					var lat = parseFloat(from_lat);
					var lng = parseFloat(from_lon);
					setMarker({lat:lat,lng:lng});
				}
				directionsDisplay.setDirections(result);
				var routes = result.routes;
				var leg = routes[0].legs;
				var lenghtText = leg[0].distance.text;
				var lenght = leg[0].distance.value;
				var duration = leg[0].duration.text;
				$('#orders-total_km').val(lenght);
				infowindow = new google.maps.InfoWindow({ content: 'Distance: '+lenghtText+'<br>Duration: '+duration });
				infowindow.open(mapto, marker);
			 }
			});
	}
  
  function setMarker(latlng,mtitle= '')
  { 
		marker = new google.maps.Marker({
			position: {
				lat: latlng.lat,
				lng: latlng.lng
			},
			map: map,
			//label: mtitle,
		});
    	return marker;
  }






