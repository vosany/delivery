<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>    
    <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
</head>
<body>
<?php $this->beginBody() ?>

<!-- The Main Wrapper -->
<div class="page">

    <!--For older internet explorer-->
    <div class="old-ie"
         style='background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;'>
        <a href="http://windows.microsoft.com/en-US/internet-explorer/..">
            <img src="/images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820"
                 alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."/>
        </a>
    </div>
    <!--END block for older internet explorer-->

    <?=$this->render('header'); ?>
    
    <!--========================================================
                              CONTENT
    =========================================================-->
    <main class="page-content text-center">

				<?php /* Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) */?>
        <?= Alert::widget() ?>
        <?= $content?>

    </main>
    <!--========================================================
                              FOOTER
    ==========================================================-->
    <footer class="page-footer">
        <div class="container-fluid">
            <div class="pull-sm-right">
                <!-- Inline list -->
                <ul class="inline-list">
                    <li><a href="#" class="icon icon-sm icon-default fa-facebook"></a></li>
                    <li><a href="#" class="icon icon-sm icon-default fa-twitter"></a></li>
                    <li><a href="#" class="icon icon-sm icon-default fa-google-plus"></a></li>
                </ul>
                <!-- END Inline list -->
            </div>
            <div class="pull-sm-left">
                <!-- RD Navbar Brand -->
                <div class="rd-navbar-brand">
                    <a href="index.html" class="brand-name">
                        <img src="/images/logo.png" alt="" width="70" height="56">
                        Delivery<span class="text-primary">Co.</span>
                    </a>
                </div>
                <!-- END RD Navbar Brand -->
                <div class="copyright">
                    © <span id="copyright-year"></span>.&nbsp;
                    <a href='index-5.html'>Privacy Policy</a>
                    More Delivery Services Website Templates at <a rel="nofollow" href="http://www.templatemonster.com/category/delivery-services-website-templates/" target="_blank">TemplateMonster.com</a>
                </div>
            </div>
        </div>
    </footer>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
