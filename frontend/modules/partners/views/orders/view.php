<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Orders */

$this->title = '35000'.$model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo "<script>var from_coordinates = '{\"lat\":\"".$model->addressfrom['lat']."\",\"lng\":\"".$model->addressfrom['lng']."\"}'</script>";
echo "<script>var to_coordinates = '{\"lat\":\"".$model->addressto['lat']."\",\"lng\":\"".$model->addressto['lng']."\"}'</script>";
?>
<div class="orders-view container">
	<input type="hidden" class="current-page-set" value="partners-orders-view">
    

    <p class="pull-right">
        <?php
        	if($model->vendor_id !== Yii::$app->user->id)
        		echo Html::a(Yii::t('app', 'Accept'), ['accept', 'id' => $model->id], ['class' => 'btn btn-success']);
        	elseif($model->status == 'pickup')
        		echo Html::a(Yii::t('app', 'Done'), ['done_order', 'id' => $model->id], ['class' => 'btn btn-info']);
        	else        		
        		echo Html::a(Yii::t('app', 'PickUp'), ['pickup', 'id' => $model->id], ['class' => 'btn btn-info']);?>
    </p>
    <h3 class="pull-left">Delivery order <?=$model->consignment?></h3>
    
    <!--div class="col-md-12">
			<div class="col-md-12">
				<div class="col-md-6">
					<div class="col-md-12">
						<div class="col-md-5">Sender name</div>
						<div class="col-md-7"><?=$model->clientfio['first_name'].' '.$model->clientfio['last_name']?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Sender address</div>
						<div class="col-md-7"><?=$model->address_from?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Sender phone</div>
						<div class="col-md-7"><?=$model->phone?></div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="col-md-12">
						<div class="col-md-5">Recipient</div>
						<div class="col-md-7"><?=$model->vendorfio['first_name'].' '.$model->vendorfio['last_name']?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Recipient address</div>
						<div class="col-md-7"><?=$model->address_to?></div>
					</div>
					<div class="col-md-12">
						<div class="col-md-5">Recipient phone</div>
						<div class="col-md-7"><?=$model->phone_to?></div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2">Price</div>
				<div class="col-md-2">Weight</div>
				<div class="col-md-2">Length</div>
				<div class="col-md-2">Width</div>
				<div class="col-md-2">Height</div>
				<div class="col-md-2">Cost</div>
			</div>
			<div class="col-md-12">
				<div class="col-md-2"><?=$model->price?></div>
				<div class="col-md-2"><?=$model->weight?></div>
				<div class="col-md-2"><?=$model->length?></div>
				<div class="col-md-2"><?=$model->width?></div>
				<div class="col-md-2"><?=$model->height?></div>
				<div class="col-md-2"><?=$model->cost?></div>
			</div>
		</div-->
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id',
            //'client_id',
			[
				'format' => 'raw',
				'attribute' => 'client_id',
				'value' => function($data){return $data->clientfio['first_name'].' '.$data->clientfio['last_name'] ;}
			],
			[
				'format' => 'raw',
				'attribute' => 'recipient_id',
				'value' => function($data){return $data->recipientfio['first_name'].' '.$data->recipientfio['last_name'] ;}
			],
            [
            	'format' => 'raw',
            	'attribute' => 'date',
            	'value' => function($data){return date("d-m-Y",$data->created);}
            ],
			'consignment',
            //'vendor_id',
            //'from_id',
            //'to_id',
			'address_from',
			'address_to',
            'phone',            
            'phone_to',
            'comment',
            'price',
            'weight',
            'length',
            'width',
            'height',
            'cost',
            [
            	'format' => 'raw',
            	'attribute' => 'type_id',
            	'value' => function($data){return $data->type_id == 1 ? 'Sender' : 'Recipient' ;}
            ]
        ],
    ]) ?>
    
    <div id="map"></div>

</div>
