<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use dosamigos\datepicker\DatePicker;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="orders-index-section">
<div class="orders-index container">
	<div class="partner-filter-date" style="width:450px;">
		<?php $form = ActiveForm::begin(['method' => 'GET']); ?>
		<div class="pull-left" style="width:220px;">
			<span class="pull-left lead"><strong>FROM&nbsp;</strong></span>
			<?= DatePicker::widget([
				'name' => 'from_date',
				'id' => 'from_date',
				'value' => (Yii::$app->request->get('from_date') ? Yii::$app->request->get('from_date') : date("d-m-Y")),
				'template' => '<div class="input-group-addon"><img src="/images/icons/calendar-icon.png" width="24" height="24" alt="phone-icon"></div>{input}',
				
				'clientOptions' => [
					'style' => 'float: left; width: 150px;',
						'autoclose' => true,
						'format' => 'dd-mm-yyyy'
					]
			]);?>
		</div>
		<div class="pull-left" style="width:220px;">
			<span class="pull-left lead"><strong>&nbsp;TO&nbsp;</strong></span>
			<?= DatePicker::widget([
				'name' => 'to_date',
				'id' => 'to_date',
				'value' => (Yii::$app->request->get('to_date') ? Yii::$app->request->get('to_date') : date("d-m-Y")),
				'template' => '<div class="input-group-addon"><img src="/images/icons/calendar-icon.png" width="24" height="24" alt="phone-icon"></div>{input}',
					'clientOptions' => [
						'autoclose' => true,
						'format' => 'dd-mm-yyyy'
					]
			]);?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
	<h3 class="text-left" style="margin-left:500px;"><?= Html::encode($this->title) ?></h3>
	<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>	<?= GridView::widget([
		'dataProvider' => $dataProvider,
		'rowOptions' => function ($model, $key, $index, $grid) {
				if($model->vendor_id == Yii::$app->user->id)
					return ['class' => 'partner-tr success'];
				else
					return ['class' => 'partner-tr'];
		},
		'filterModel' => $searchModel,
		'columns' => [
			//['class' => 'yii\grid\SerialColumn'],

			[
				'class' => 'frontend\components\ButtonUpdateDelete',
				'template'=>'{partner_view}',
				'contentOptions' => ['style' => 'width: 70px;','class' => 'hidden visible-xs'],
				'headerOptions' => ['class' => 'hidden visible-xs'],
				'filterOptions' => ['class' => 'hidden visible-xs'],
				'header'=>'Actions', 
			],
			[
				'attribute' => 'client_id',
				'value' => function($data){ return $data->clientfio['first_name'].' '.$data->clientfio['last_name'];},
				'contentOptions' => ['class' => 'hidden visible-md visible-lg'],
				'headerOptions' => ['class' => 'hidden visible-md visible-lg'],
				//'filter' => Html::activeDropDownList($searchModel, 'client_id', ArrayHelper::map(TypeData::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '   ']),
			],
			[
				'attribute' => 'from_id',
				'value' => function($data){ return $data->addressfrom['name'];},
				//'filter' => Html::activeDropDownList($searchModel, 'from_id', ArrayHelper::map(TypeData::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '   ']),
			], 
			[
				'attribute' => 'to_id',
				'value' => function($data){ return $data->addressto['name'];},
				//'filter' => Html::activeDropDownList($searchModel, 'to_id', ArrayHelper::map(TypeData::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '   ']),
			], 
			[
				'attribute' => 'price',
				'contentOptions' => ['class' => 'hidden visible-md visible-lg'],
				'headerOptions' => ['class' => 'hidden visible-md visible-lg'],
			],
			 'cost',
			[
				'label' => "Pickup distance",
				'format'=>"raw",
				'value' => function($data){return round($data->mindf)." km.";}
			],
			[
				'label' => "End point distance",
				'format'=>"raw",
				'value' => function($data){return round($data->mindt)." km.";}
			],
			[
				'label' => "Date",
				'format'=>"raw",
				'value' => function($data){return date("d-m-Y",$data->created);}
			],
			// 'type_id',
			[
				'class' => 'frontend\components\ButtonUpdateDelete',
				'template'=>'{partner_view}{order_type}',
				'contentOptions' => ['style' => 'width: 200px;','class' => 'text-left'],
				'header'=>'Actions', 
			],
		],
		'summary'=>'',
	]); ?>
<?php Pjax::end(); ?></div>
</section>
