<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Profile';
?>
<script>var partnerPointSet = <?=json_encode($partnersPoints)?>;</script>
<div class="container">
	<div class="partner-profile">
		<h3>Profile</h3>
		<?php $form = ActiveForm::begin(); ?>
			<div class="col-md-6 col-md-offset-3">
				<?=$form->field($userprofile,'first_name')->textInput(['placeholder'=>'First name'])->label(false)?>
				<?=$form->field($userprofile,'last_name')->textInput(['placeholder'=>'Last name'])->label(false)?>
				<?=$form->field($partners,'license')->textInput(['placeholder'=>'License'])->label(false)?>
				<?=$form->field($partners,'carname')->textInput(['placeholder'=>'Model car'])->label(false)?>
				<?=$form->field($partners,'distance')->textInput(['placeholder'=>'Distance from'])->label(false)?>
				<?=$form->field($partners,'maxsize')->textInput(['placeholder'=>'Max Size'])->label(false)?>
				<?=$form->field($partners,'maxweight')->textInput(['placeholder'=>'Max weight'])->label(false)?>
				<div>
					<strong>Set points on the map, where your start route,<br> end route and middleway points to pickup orders</strong>
				</div>
				<div class="hidden">
					<p>Start/End Points</p>
					<?=$form->field($partners,'start_point')->textInput(['id'=>'partner-start-point','placeholder'=>'Start Point / Example City,Street,Building'])->label(false)?>
					<?=$form->field($partners,'end_point')->textInput(['id'=>'partner-end-point','placeholder'=>'End Point / Example City,Street,Building'])->label(false)?>
				</div>
				<div class="partners-set-route">
					<?php $pointNum=1; ?>
					<?php foreach($partnersPoints as $point): ?>
						<input type="hidden" class="PartnersPoints pPoint<?=$pointNum?>" name="PartnersPoints[<?=$pointNum?>][lat]" value="<?=$point['lat']?>">
				        <input type="hidden" class="PartnersPoints pPoint<?=$pointNum?>" name="PartnersPoints[<?=$pointNum?>][lng]" value="<?=$point['lng']?>">
				        <?php $pointNum++; ?>
					<?php endforeach; ?>
				</div>
				<div class="fast-route">
					<button class="btn dubai">Dubai</button>
					<button class="btn abudabi">Abu Dabi</button>
					<?=Html::submitButton('Save',['class' => 'btn btn-danger'])?>
				</div>
			</div>
			
			<div id="map"></div>
		<?php ActiveForm::end(); ?>
	</div>
</div>
