<?php

namespace frontend\modules\partners\controllers;

use Yii;
use common\models\OrdersSearch;
use common\models\Orders;
use common\models\Sms;
use common\models\Partners;
use common\models\PartnersPoints;
use common\models\Userprofile;
use common\models\Coordinates;

class OrdersController extends \common\components\BaseController
{

	public function beforeAction($action)
	{			
		if ($action->id == 'setposition') {
			$this->enableCsrfValidation = false;
		}
	
		return parent::beforeAction($action);
	}
	public function actionIndex()
	{
		//$sms = new Sms();
		//$sms->send('test');
		//$sms->getStatus();
		//$sms = Sms::findOne(4)->getStatus();
		//$distance = $this->pointDistance(24.488413,54.630539,24.375689,54.731529);
		$from_date = Yii::$app->request->get('from_date') ? Yii::$app->request->get('from_date') : date("d-m-Y");
		$to_date = Yii::$app->request->get('to_date') ? Yii::$app->request->get('to_date') : date("d-m-Y");
		
		$searchModel = new OrdersSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		
		$dataProvider->query->andWhere(['in','vendor_id',[Yii::$app->user->id,'',NULL,0,'0']]);
		$dataProvider->query->andWhere(['!=','status', 'done']);
		$dataProvider->query->andWhere(['>=','created', strtotime($from_date)]);
		$dataProvider->query->andWhere(['<=','created', strtotime($to_date)+86400]);
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	
	public function actionDone_order($id)
	{
		if($model = $this->findModel($id))
		{
			$model->status = "done";
			$model->save();
			return $this->redirect(['index']);
		}
	}
	
	public function actionPickup($id)
	{
		if($model = $this->findModel($id))
		{
			$model->status = "pickup";
			$model->save();
			return $this->redirect(['view','id' => $id]);
		}
	}
	
	public function actionView($id)
	{
		$model = $this->findModel($id);
		if(!$model)
			return $this->redirect(['/partners/orders']);
		if($model->consignment == '350000')
			$model->consignment = '350000'.$model->id;
		return $this->render('view', [
			'model' => $model,
		]);
	}
	
	public function actionSetposition()
	{
		if(Yii::$app->user->id)
		{
			if($partner = Partners::findOne(['user_id' => Yii::$app->user->id]))
			{
				$partner->current_lat = Yii::$app->request->post('lat');
				$partner->current_lon = Yii::$app->request->post('lng');
				$partner->save();
				echo "success";
			}
		}
	}
	
	public function actionAccept($id)
	{
		$model = OrdersSearch::findOne($id);
		$model->vendor_id = Yii::$app->user->id;
		$model->status = 'in-progress';
		if($model->save())
			echo 'Delivery was accepted';
		return $this->redirect(Yii::$app->request->referrer);
	}
	
	protected function findModel($id)
	{
		if (($model = Orders::findOne($id)) !== null && (!(bool)$model->vendor_id || $model->vendor_id == Yii::$app->user->id)) {
				if($model->client_id && $userprofile = Userprofile::find()->where(['user_id' => $model->client_id])->one()){
					$model->first_name = $userprofile->first_name;
					$model->last_name = $userprofile->last_name;
				}
				if($model->recipient_id && (int)$model->recipient_id > 0 && $userprofile = Userprofile::find()->where(['user_id' => $model->recipient_id])->one()){
					$model->first_name_to = $userprofile->first_name;
					$model->last_name_to = $userprofile->last_name;
				}
				if($model->from_id)
					$model->address_from = Coordinates::findOne($model->from_id)->address;
				if($model->to_id)
					$model->address_to = Coordinates::findOne($model->to_id)->address;
			return $model;
		} else {
			return false;
		}
	}
	
	

}
