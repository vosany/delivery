<?php

namespace frontend\modules\partners\controllers;

use Yii;
use yii\web\Controller;
use common\models\OrdersSearch;

/**
 * Default controller for the `partners` module
 */
class DeliveryController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    
    public function actionIndex()
    {
    	//$sms = new Sms();
    	//$sms->send('test');
    	//$sms->getStatus();
    	//$sms = Sms::findOne(4)->getStatus();
    	//$distance = $this->pointDistance(24.488413,54.630539,24.375689,54.731529);
				//echo "<pre>";print_r($distance);exit;
        $searchModel = new OrdersSearch();
        $searchModel->vendor_id = Yii::$app->user->id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id)
    {
    	$model = OrdersSearch::findOne($id);
    	if($model->consignment == '350000')
    		$model->consignment = '350000'.$model->id;
        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
