<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
$this->title = 'Profile';
?>
<div class="container">
	<h3 class="pull-left">Partner profile</h3>
	<?php $form = ActiveForm::begin(); ?>
		<div class="col-md-12">
			<div class="col-md-6">
				<?=$form->field($userprofile,'first_name')->textInput()?>
				<?=$form->field($userprofile,'last_name')->textInput()?>
			</div>
			<div class="col-md-6">
				<?=$form->field($partners,'license')->textInput()?>
				<?=$form->field($partners,'carname')->textInput()?>
			</div>
			<div class="col-md-4">
				<?=$form->field($partners,'distance')->textInput()?>
			</div>
			<div class="col-md-4">
				<?=$form->field($partners,'maxsize')->textInput()?>
			</div>
			<div class="col-md-4">
				<?=$form->field($partners,'maxweight')->textInput()?>
			</div>
		</div>
		<div class="col-md-12">
			<?=Html::submitButton('Save',['class' => 'btn btn-success pull-right'])?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
