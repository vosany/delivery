<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Cities;
$this->title = 'Profile';
?>
<div class="container">
	<h3 class="pull-left">Partner profile</h3>
	<?php $form = ActiveForm::begin(); ?>
		<div class="row">
	        <div class="col-md-6">  
	                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
	                <?= $form->field($profile, 'first_name') ?>		
	                <?= $form->field($model, 'password')->passwordInput() ?>	
	                <?= $form->field($profile, 'street') ?>	
	        </div>
	        <div class="col-md-6">
				<?= $form->field($model, 'email') ?>
				<?= $form->field($profile, 'last_name') ?>
			    <?= $form->field($profile, 'city_id')->dropdownList(
			    		Cities::find()->select(['name', 'id'])->indexBy('id')->column(),
			    		['prompt'=>Yii::t('app','City')]) ?>	
			   	<div class="col-md-6">
			   		<?= $form->field($profile, 'bulding') ?>
			   	</div>
			   	<div class="col-md-6">
			   		<?= $form->field($profile, 'apartment') ?>
			   	</div>
	        </div>
			<div class="col-md-6">
				<?=$form->field($profile,'first_name')->textInput()?>
				<?=$form->field($profile,'last_name')->textInput()?>
			</div>
			<div class="col-md-6">
				<?=$form->field($partners,'license')->textInput()?>
				<?=$form->field($partners,'carname')->textInput()?>
			</div>
			<div class="col-md-4">
				<?=$form->field($partners,'distance')->textInput()?>
			</div>
			<div class="col-md-4">
				<?=$form->field($partners,'maxsize')->textInput()?>
			</div>
			<div class="col-md-4">
				<?=$form->field($partners,'maxweight')->textInput()?>
			</div>
		</div>
		<div class="col-md-12">
			<?=Html::submitButton('Signup',['class' => 'btn btn-success pull-right'])?>
		</div>
	<?php ActiveForm::end(); ?>
</div>
