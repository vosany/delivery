<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Cities;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
	<div class="site-signup">
	    <h3><?= Html::encode($this->title) ?></h3>
	    <p>Please fill out the following fields to signup:</p>
	    
	    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
	    <div class="row">
	        <div class="col-md-6">  
	                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
	                <?= $form->field($profile, 'first_name') ?>		
	                <?= $form->field($model, 'password')->passwordInput() ?>	
	                <?= $form->field($profile, 'street') ?>
	
	
	                <div class="form-group">
	                    <?= Html::submitButton('Signup', ['class' => 'btn btn-success', 'name' => 'signup-button']) ?>
	                </div>
	
	        </div>
	        <div class="col-md-6">
				<?= $form->field($model, 'email') ?>
				<?= $form->field($profile, 'last_name') ?>
			    <?= $form->field($profile, 'city_id')->dropdownList(
			    		Cities::find()->select(['name', 'id'])->indexBy('id')->column(),
			    		['prompt'=>Yii::t('app','City')]) ?>	
			   	<div class="col-md-6">
			   		<?= $form->field($profile, 'bulding') ?>
			   	</div>
			   	<div class="col-md-6">
			   		<?= $form->field($profile, 'apartment') ?>
			   	</div>
	        </div>
	    </div>
	    <?php ActiveForm::end(); ?>
	</div>
</div>

