<?php

namespace frontend\views;

use Yii;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use common\models\Cities;
use common\models\Partners;

$this->title = 'Profile';
?>
<script>var partnerPointSet = <?=json_encode($partnersPoints)?>;</script>
<div class="container">
	<div class="row">
		<div class="col-md-6 col-md-offset-3 navigate-btn">
			<button class="btn navigate-button active" data-name="client">Client</button>
			<button class="btn navigate-button" data-name="partner">Partner</button>
		</div>
		<div class="col-md-12">
			<h3><?=$this->title?></h3>		
		</div>
	</div>
	<div class="user-profile">
		<div class="row">
			<?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
				<div class="col-md-6 col-md-offset-3">
					<div class="col-md-12">
						<div class="pull-left">
							<img src="/images/icons/user-gear-icon.png" width="60" height="60" alt="">
						</div>
						<div class="pull-right">
							<?= $form->field($profile, 'first_name')->textInput(['placeholder'=>'First name',])->label(false) ?>
						</div>							
					</div>
					<div class="col-md-12">
						<div class="pull-left">
							<img src="/images/icons/gears-icon.png" width="60" height="60" alt="">
						</div>
						<div class="pull-right">
							<?= $form->field($profile, 'last_name')->textInput(['placeholder'=>'Last name',])->label(false) ?>
						</div>							
					</div>
					<div class="col-md-12">
						<div class="pull-left">
							<img src="/images/icons/profile-phone-icon.png" width="60" height="60" alt="">
						</div>
						<div class="pull-right">
							<?= $form->field($profile, 'phone')->textInput(['placeholder'=>'Telephone'])->label(false) ?>
						</div>							
					</div>
					<div class="col-md-12">
						<div class="pull-left">
							<img src="/images/icons/city-icon.png" width="60" height="60" alt="">
						</div>
						<div class="pull-right">
							<?= $form->field($profile, 'city_id')->dropdownList(
							Cities::find()->select(['name', 'id'])->indexBy('id')->column(),
							['prompt'=>Yii::t('app','City')])->label(false) ?>
						</div>							
					</div>
				</div>
				<div class="col-md-6 col-md-offset-3">
					<div class="col-md-12">
						<div class="pull-left">
							<img src="/images/icons/place-icon.png" width="60" height="60" alt="">
						</div>
						<div class="pull-right">							
							<div class="col-md-6">						
								<?= $form->field($profile, 'street')->textInput(['placeholder'=>'Street'])->label(false) ?>
							</div>
						   	<div class="col-md-3">
						   		<?= $form->field($profile, 'bulding')->textInput(['placeholder'=>'Building'])->label(false) ?>
						   	</div>
						   	<div class="col-md-3">
						   		<?= $form->field($profile, 'apartment')->textInput(['placeholder'=>'App'])->label(false) ?>
						   	</div>
						</div>							
					</div>
				</div>
				<div class="col-md-6 col-md-offset-3">
					<?= Html::submitButton('Save', ['class' => 'btn btn-danger', 'name' => 'signup-button']) ?>
				</div>
				<div class="form-group pull-right hidden">
					<br>
					<?php if(Partners::find()->where(['user_id' => Yii::$app->user->id])->one())
						echo Html::a("Partner",['partners/profile'],['class' => 'btn btn-info']);
					else
						echo Html::a("Partnership",['become_partner'],['class' => 'btn btn-warning']);
					?>
					
				</div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
	<div class="partner-profile" style="display:none;">
		<?php $form = ActiveForm::begin(); ?>
			<div class="col-md-6 col-md-offset-3">
				<div class="col-md-12">
					<div class="pull-left">
						<img src="/images/icons/user-gear-icon.png" width="60" height="60" alt="">
					</div>
					<div class="pull-right">
						<?= $form->field($profile, 'first_name')->textInput(['placeholder'=>'First name',])->label(false) ?>
					</div>							
				</div>
				<div class="col-md-12">
					<div class="pull-left">
						<img src="/images/icons/gears-icon.png" width="60" height="60" alt="">
					</div>
					<div class="pull-right">
						<?= $form->field($profile, 'last_name')->textInput(['placeholder'=>'Last name',])->label(false) ?>
					</div>							
				</div>
				
				<div class="col-md-12">
					<div class="pull-left">
						<img src="/images/icons/license-icon.png" width="60" height="60" alt="">
					</div>
					<div class="pull-right">
						<?=$form->field($partners,'license')->textInput(['placeholder'=>'License'])->label(false)?>
					</div>							
				</div>
				<div class="col-md-12">
					<div class="pull-left">
						<img src="/images/icons/car-icon.png" width="60" height="60" alt="">
					</div>
					<div class="pull-right">
						<?=$form->field($partners,'carname')->textInput(['placeholder'=>'Model car'])->label(false)?>
					</div>							
				</div>
				<div class="col-md-12">
					<div class="pull-left">
						<img src="/images/icons/places-icon.png" width="60" height="60" alt="">
					</div>
					<div class="pull-right">
						<?=$form->field($partners,'distance')->textInput(['placeholder'=>'Distance from'])->label(false)?>
					</div>							
				</div>
				<div class="col-md-12">
					<div class="pull-left">
						<img src="/images/icons/size-icon.png" width="60" height="60" alt="">
					</div>
					<div class="pull-right">
						<?=$form->field($partners,'maxsize')->textInput(['placeholder'=>'Max Size'])->label(false)?>
					</div>							
				</div>
				<div class="col-md-12">
					<div class="pull-left">
						<img src="/images/icons/weight-icon.png" width="60" height="60" alt="">
					</div>
					<div class="pull-right">
						<?=$form->field($partners,'maxweight')->textInput(['placeholder'=>'Max weight'])->label(false)?>
					</div>							
				</div>
				<div>
					<strong>Set points on the map, where your start route,<br> end route and middleway points to pickup orders</strong>
				</div>
				<div class="hidden">
					<p>Start/End Points</p>
					<?=$form->field($partners,'start_point')->textInput(['id'=>'partner-start-point','placeholder'=>'Start Point / Example City,Street,Building'])->label(false)?>
					<?=$form->field($partners,'end_point')->textInput(['id'=>'partner-end-point','placeholder'=>'End Point / Example City,Street,Building'])->label(false)?>
				</div>
				<div class="partners-set-route">
					<?php $pointNum=1; ?>
					<?php foreach($partnersPoints as $point): ?>
						<input type="hidden" class="PartnersPoints pPoint<?=$pointNum?>" name="PartnersPoints[<?=$pointNum?>][lat]" value="<?=$point['lat']?>">
				        <input type="hidden" class="PartnersPoints pPoint<?=$pointNum?>" name="PartnersPoints[<?=$pointNum?>][lng]" value="<?=$point['lng']?>">
				        <?php $pointNum++; ?>
					<?php endforeach; ?>
				</div>
				<div class="fast-route">
					<button class="btn dubai">Dubai</button>
					<button class="btn abudabi">Abu Dabi</button>
					<?=Html::submitButton('Save',['class' => 'btn btn-danger'])?>
				</div>
			</div>
			
			<div class='col-md-12'>
				<div id="map"></div>
			</div>
		<?php ActiveForm::end(); ?>
	</div>
	</div>

</div>