<?php 
namespace frontend\views;
use Yii;
use common\models\Partners;

$this->title = 'Tracking ' .$model->consignment;
$partner = Partners::findOne(['user_id' => $model->vendor_id]);

?>
<h3><?=$this->title?></h3>
</br>
<div class="container">
	<div class="col-md-12">
<?php if($partner): ?>
		<div id="map"></div>
		<script type="text/javascript">
		var position = {
					lat : <?=$partner->current_lat?>,
					lon : <?=$partner->current_lon?>
				}
		</script>
<?php else: ?>
	<h4>We are looking for courier for you.</h4>
<?php endif; ?>
	</div>
</div>
