<?php

use yii\db\Migration;

class m170624_131815_userprofile_alter extends Migration
{
    public function up()
    {				
				$this->addColumn("{{%userprofile}}", "phone", $this->string()->defaultValue(0)->notNull());
    }

    public function down()
    {
        //echo "m170624_131815_userprofile_alter cannot be reverted.\n";
				$this->dropColumn("{{%userprofile}}", "phone");
        //return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
