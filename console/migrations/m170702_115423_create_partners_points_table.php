<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partners_points`.
 */
class m170702_115423_create_partners_points_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('partners_points', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer()->notNull(),
            'lat' => $this->string()->notNull(),
            'lng' => $this->string()->notNull(),
        ]);

        // creates index for column `partner_id`
        $this->createIndex(
            'idx-partners_points-partner_id',
            'partners_points',
            'partner_id'
        );

        // add foreign key for table `partners`
        $this->addForeignKey(
            'fk-partners_points-partner_id',
            'partners_points',
            'partner_id',
            'partners',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('partners_points');
    }
}
