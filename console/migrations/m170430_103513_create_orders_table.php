<?php

use yii\db\Migration;

/**
 * Handles the creation of table `orders`.
 */
class m170430_103513_create_orders_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer()->notNull(),
            'vendor_id' => $this->integer()->defaultValue(0),
            'consignment' => $this->string(),
            'from_id' => $this->integer(),
            'to_id' => $this->integer(),
            'phone' => $this->string()->notNull(),
            'comment' => $this->string(),
            'price' => $this->integer(),
            'weight' => $this->integer()->defaultValue(1),
            'length' => $this->integer(),
            'width' => $this->integer(),
            'height' => $this->integer(),
						'total_km' => $this->integer(),
            'cost' => $this->integer(),
            'type_id' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('orders');
    }
}
