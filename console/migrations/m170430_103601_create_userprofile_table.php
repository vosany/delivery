<?php

use yii\db\Migration;

/**
 * Handles the creation of table `userprofile`.
 */
class m170430_103601_create_userprofile_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('userprofile', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'city_id' => $this->string(),
            'street' => $this->string(),
            'bulding' => $this->string(),    
            'apartment' => $this->string(),           
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('userprofile');
    }
}
