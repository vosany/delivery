<?php

use yii\db\Migration;

/**
 * Handles the creation of table `coordinates`.
 */
class m170430_103522_create_coordinates_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('coordinates', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'lat' => $this->decimal(10,6)->notNull()->defaultValue(0),
            'lng' => $this->decimal(10,6)->notNull()->defaultValue(0),
            'type_id' => $this->integer(),
            'name' => $this->string(),
            'address' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('coordinates');
    }
}
