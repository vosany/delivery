<?php
/* @var $this yii\web\View */
?>


<div class="container col-md-12">
	<form class="form-inline" action="" method="post">
	<?php foreach($model as $k => $val): ?>
		<div class="col-md-12">
			<div class="form-group col-md-3"></div>
			<div class="form-group col-md-7">
				<div class="col-md-4">
					<label class="control-label" for="orders-first_name"><?=$val->name?></label>
				</div>
				<div class="col-md-8">
					<input type="text" class="form-control" name="Settinds[<?=$k?>][value]" value="<?=$val->value?>" maxlength="255">
				</div>
			</div>
			<div class="form-group col-md-2"></div>
		</div>
	<?php endforeach;?>
	<div class="col-md-12">
		<div class="col-md-3 col-md-offset-7">
			<input type="submit" class="btn btn-success" value="Save">
		</div>
	</div>
	</form>
</div>
