<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Partners');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="orders-index-section">
<div class="orders-index container">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?php Pjax::begin(); ?>    
	<?= GridView::widget([
        'dataProvider' => $partners,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            
            
            [
				
				'value' => function($data){ return $data->profile['first_name'].' '.$data->profile['last_name'];},
				//'filter' => Html::activeDropDownList($searchModel, 'client_id', ArrayHelper::map(TypeData::find()->asArray()->all(), 'id', 'name'),['class'=>'form-control','prompt' => '   ']),
            ],
            'distance',
            
            [
				'attribute' => 'maxweight',
				'value' => function($data){ return $data->maxweight ? $data->maxweight : 'Not set';},
			], 
            [
				'attribute' => 'maxsize',
				'value' => function($data){ return $data->maxsize ? $data->maxsize : 'Not set';},
			], 
             'start_point',
             'end_point',
            [
            	//'class' => 'frontend\components\ButtonUpdateDelete',
            	'class' => 'yii\grid\ActionColumn',
        		'template'=>'{view} {update}',
        		'contentOptions' => ['style' => 'width: 100px;'],
        		'header'=>'Actions', 
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
</section>
