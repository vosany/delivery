<?php

namespace backend\controllers;
use Yii;
use common\models\Partners;
use yii\data\ActiveDataProvider;

class PartnersController extends \common\components\BaseController
{
    public function actionIndex()
    {
    	$partners = Partners::find()->with('user')->with('profile');
    	
    	$dataProvider = new ActiveDataProvider([
            'query' => $partners,
        ]);
    	//var_dump($partners[0]->user);
        return $this->render('index',['partners'=>$dataProvider]);
    }
    
    public function actionView()
    {
      
    }
    
    public function actionUpdate()
    {
      
    }

}
