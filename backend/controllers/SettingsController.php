<?php

namespace backend\controllers;
use common\models\Settings;

class SettingsController extends \common\components\BaseController
{
    public function actionIndex()
    {
    	$model = Settings::find()->all();
        return $this->render('index',['model' => $model]);
    }

}
